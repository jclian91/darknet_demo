This is a simple demo for object detection with darknet, for more information, please refer to the website: http://pjreddie.com/darknet .

for how to use darknet for object detection, please refer to the blog: https://blog.csdn.net/jclian91/article/details/106063161 .

A smiple demo for object detection with image:

![](https://gitlab.com/jclian91/darknet_demo/-/raw/master/predictions.jpg)
